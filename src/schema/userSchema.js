import { Schema } from "mongoose";

let userSchema = Schema(
  {
    name: {
      type: String,
      required: [true, "name field is required."],
    },
    email: {
      type: String,
      unique: true,
      required: [true, "age field is required."],
    },
    isMarried: {
      type: Boolean,
      required: [true, "isMarried field is required."],
    },
    password:{
      // bcrypt(package) : it is used for hashing
      type:String,
      required: [true, "password field is required."],
    },
    profileImage :{
      type:String,
      required:[true, "profileImage field is required."]
    },
    resume:{
      type:String,
      required:[true, "resume field is required"]
    }
  }, 
  {
    timestamps:true
  });

export default userSchema;
