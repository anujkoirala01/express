import { Schema } from "mongoose";

// schema means defining content of document
const productSchema = Schema(
  {
    name: {
      type: String,
      required: [true, "name field is required"],
      trim: true,
    },
    price: {
      type: Number,
      required: [true, "price field is required"],
      trim: true,
    },
    quantity: {
      type: Number,
      required: [true, "quantity field is required"],
      trim: true,
    }
  },
  {
    timestamps: true,
  }
);

export default productSchema;
