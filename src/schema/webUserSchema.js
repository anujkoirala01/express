import { Schema } from "mongoose";


//regex : Regular Expression
let webUserSchema = Schema(
  {
    name: {
      type: String,
      required: [true, "name field is required."],
      minLength:2,

    },
    age: {
      type: Number,
      required: [true, "age field is required."],
      min:18,
      max:70,
      validate: (value)=>{
        if(value===30)
        {
          throw new Error("Age cannot be 30")
        }

      }
    },
    password: {
      type: String,
      required: [true, "password field is required."],
    },
    phoneNumber: {
      type: Number,
      required: [true, "phone number field is required."],
      minLength:1000000000,
      maxLength:9999999999,
    },
    roll: {
      type: Number,
      required: [true, "roll field is required."],
    },
    isMarried: {
      type: Boolean,
      required: [true, "isMarried field is required."],
    },
    spouseName: {
      type: String,
      required: [true, "spouseName field is required."],
    },
    email: {
      type: String,
      required: [true, "Email field is required."],
    },
    gender: {
      type: String,
      required: [true, "gender field is required."],
    },
    dob: {
      type: Date,
      required: [true, "dob field is required."],
    },
    location: {
      country: {
        type: String,
        required: [true, "country is required"],
      },
      exactLocation: {
        type: String,
        required: [true, "exactLocation is required"],
      },
    },
    favTeacher: [
      { type: String, 
        required: [true, "favTeacher field is required"] },
    ],
    favSubject:[
      {
        bookName:{
          type:String,
          required:[true,"bookName field is required"]
        },
        bookAuthor:{
          type:String,
          required:[true,"bookAuthor field is required"]
        }
      }
    ]
  },
  {
    timestamps: true,
  }
);

export default webUserSchema;
