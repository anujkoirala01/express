import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import bookSchema from "./bookSchema.js";
import userSchema from "./userSchema.js";
import traineeSchema from "./traineeSchema.js";
import collegeSchema from "./collegeSchema.js";
import webUserSchema from "./webUserSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Student = model("Student", studentSchema);

export let Teacher = model("Teacher", teacherSchema);

export let Book = model("Book", bookSchema);

export let User = model("User", userSchema)

export let Trainee = model("Trainee", traineeSchema)

export let College = model("College", collegeSchema)

export let Webuser = model("Webuser", webUserSchema)

export let Product = model("Product", productSchema)

export let Review = model("Review", reviewSchema)

// model name --> singular and first letter capital
// and same as the variable name
