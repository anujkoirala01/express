import { Router } from "express";
import { Book } from "../schema/model.js";


let bookRouter = Router()

bookRouter.route("/")
.post(async(req,res,next)=>{
    let bookData = req.body

    try {
        let result = await Book.create(bookData)
        res.json({
            success:true,
            message:"Book Created Successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

})
.get(async(req,res,next)=>{

    try {
        let result = await Book.find({})
        res.json({
            success:true,
            message:"Book Read Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})

bookRouter.route("/:booksId")
.delete(async(req,res,next)=>{
    let bookId = req.params.booksId

    try {
        let result = await Book.findByIdAndDelete(bookId)
        res.json({
            success:true,
            message:"Book Deleted Successfully",
            result: result
        })        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})

export default bookRouter