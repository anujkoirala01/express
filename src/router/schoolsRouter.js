import { Router, json } from "express";

let schoolsRouter = Router()

schoolsRouter
.route("/")
.post((req,res,next)=>{
    res.json({
        success: true,
        message: "School created successfully"
    })
})
.get((req,res,next)=>{
    res.json({
        success: true,
        message: "School read successfully"
    })
})
.patch((req,res,next)=>{
    res.json({
        success: true,
        message: "School updated successfully"
    })
})
.delete((req,res,next)=>{
    res.json({
        success: true,
        message: "School deleted successfully"
    })
})


export default schoolsRouter