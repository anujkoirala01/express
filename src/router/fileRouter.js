import { Router } from "express";
import upload from "../middleware/upload.js";
import { handleFile } from "../controller/fileController.js";

let fileRouter = Router();


fileRouter
  .route("/") // localhost:8000
  .post(upload.array("document",4), handleFile)

export default fileRouter;
