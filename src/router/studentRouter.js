import { Router } from "express";
import { Student } from "../schema/model.js";

let studentRouter = Router();

studentRouter
  .route("/")
  .post(async (req, res, next) => {
    let studentData = req.body;

    try {
      let result = await Student.create(studentData);
      res.json({
        success: true,
        message: "Student created successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res, next) => {
    try {
      let result = await Student.find({});
      res.json({
        success: true,
        message: "Student read successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

studentRouter
  .route("/:studentsId")
  .delete(async (req, res, next) => {
    let studentId = req.params.studentsId;

    try {
      let result = await Student.findByIdAndDelete(studentId);
      res.json({
        success: true,
        message: "Student deleted successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res, next) => {
    let studentId = req.params.studentsId;

    try {
      let result = await Student.findById(studentId);
      res.json({
        success: true,
        message: "Student Read by ID Successfully",
        result: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async(req,res,next)=>{
    let studentId = req.params.studentsId
    let studentData = req.body

    try {
      let result = await Student.findByIdAndUpdate(studentId,studentData)
      res.json({
        success: true,
        message: "Student updated by ID Successfully",
        result: result,
      })
      
    } catch (error) {
      res.json({
        success:false,
        message:error.message
      })
    }
  })

export default studentRouter;
