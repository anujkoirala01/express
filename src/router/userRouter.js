import { Router } from "express";
import {
  createUser,
  deleteUser,
  readUser,
  readUserDetail,
  updateUser,
  loginUser,
  myProfile,
} from "../controller/userController.js";

let userRouter = Router();

userRouter.route("/").post(createUser).get(readUser);

userRouter.route("/login").post(loginUser);

// Error: "Cast to ObjectId failed for value \"my-profile\" (type string) at path \"_id\" for model \"User\""
// /my-profile is not a id

userRouter.route("/my-profile").get(myProfile);

// Always place dynamic routes at last 
userRouter
  .route("/:usersId")
  .delete(deleteUser)
  .get(readUserDetail)
  .patch(updateUser);

export default userRouter;
