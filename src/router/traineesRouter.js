import { Router, json } from "express";
import { createTrainee, deleteTrainee, readTrainee, readTraineeDetail, updateTrainee } from "../controller/traineeController.js";

let traineesRouter = Router();

traineesRouter.route("/")
.post(createTrainee)
.get(readTrainee);

traineesRouter.route("/:traineesId")
.get(readTraineeDetail)
.delete(deleteTrainee)
.patch(updateTrainee);

export default traineesRouter;
