import { Router } from "express";
import { createCollege, deleteCollege, readCollege, readCollegeDetail, updateCollege } from "../controller/collegeController.js";

let collegeRouter = Router();

collegeRouter.route("/").post(createCollege).get(readCollege);

collegeRouter
  .route("/:collegesId")
  .delete(deleteCollege)
  .get(readCollegeDetail)
  .patch(updateCollege);

export default collegeRouter;
