import { Router } from "express";
import { createReview, deleteReview, readReview, readReviewDetail, updateReview } from "../controller/reviewController.js";


let reviewRouter = Router();

reviewRouter.route("/").post(createReview).get(readReview);

reviewRouter
  .route("/:reviewsId")
  .delete(deleteReview)
  .get(readReviewDetail)
  .patch(updateReview);

export default reviewRouter;
