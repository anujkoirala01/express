import { Router } from "express";
import { createProduct, deleteProduct, readProduct, readProductDetail, updateProduct } from "../controller/productController.js";


let productRouter = Router();

productRouter.route("/").post(createProduct).get(readProduct);

productRouter
  .route("/:productsId")
  .delete(deleteProduct)
  .get(readProductDetail)
  .patch(updateProduct);

export default productRouter;
