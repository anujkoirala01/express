import { Router, json } from "express";

let bikeRouter = Router();

bikeRouter.route("/").post(
  (req, res, next) => {
    console.log("I am middleware 1");

    next("a");
  },
  (err, req, res, next) => {
    console.log("I am error middleware 1");
    next()
  },
  (req, res, next) => {
    console.log("I am middleware 2");
    next();
  },
  (err, req, res, next) => {
    console.log("I am error middleware 2");
  },
  (req, res, next) => {
    console.log("I am middleware 3");
  }
);

// Middleware is basically a function(which has req,res and next) inside CRUD

export default bikeRouter;
