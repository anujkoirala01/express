import { Router } from "express";
import { Teacher } from "../schema/model.js";


let teacherRouter = Router()

teacherRouter.route("/")
.post(async(req,res,next)=>{
    let teacherData = req.body

    try {
        let result = await Teacher.create(teacherData)
        res.json({
            success:true,
            message: "Teacher Created Successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

})
.get(async(req,res,next)=>{

    try {
        let result = await Teacher.find({})
        res.json({
            success:true,
            message:"Teacher Read Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })   
    }
})

teacherRouter.route("/:teachersId")
.delete(async(req,res,next)=>{
    let teacherId = req.params.teachersId

    try {
        let result = await Teacher.findByIdAndDelete(teacherId)
        res.json({
            success:true,
            message:"Teacher Deleted Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message


        })
        
    }
})

export default teacherRouter