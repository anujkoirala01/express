import { Router, json } from "express";
import { createWebuser, deleteWebuser, readWebuser, readWebuserDetail, updateWebuser } from "../controller/webUserController.js";

let webuserRouter = Router();

webuserRouter.route("/")
.post(createWebuser)
.get(readWebuser);

webuserRouter.route("/:webuserId")
.get(readWebuserDetail)
.delete(deleteWebuser)
.patch(updateWebuser);

export default webuserRouter;
