import { Router } from "express";

let vehiclesRouter = Router()

vehiclesRouter
.route("/")
.post((req,res,next)=>{
    res.json({
        success:true,
        message:"Vehicles created successfully"
    })
})
.get((req,res,next)=>{
    res.json({
        success:true,
        message:"Vehicles read successfully"
    })
})
.patch((req,res,next)=>{
    res.json({
        success:true,
        message:"Vehicles updated successfully"
    })
})
.delete((req,res,next)=>{
    res.json({
        success:true,
        message:"Vehicles deleted successfully"
    })
})

export default vehiclesRouter