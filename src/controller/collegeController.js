import { College } from "../schema/model.js";

export let createCollege = async (req, res, next) => {
  let collegeData = req.body;

  try {
    let result = await College.create(collegeData);
    res.json({
      success: true,
      message: "College created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readCollege = async (req, res, next) => {
  try {
    let result = await College.find({});
    res.json({
      success: true,
      message: "College read successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readCollegeDetail = async (req, res, next) => {
  let collegeId = req.params.collegesId;

  try {
    let result = await College.findById(collegeId);
    res.json({
      success: true,
      message: "College Read by ID Successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateCollege = async (req, res, next) => {
  let collegeId = req.params.collegesId;
  let collegeData = req.body;

  try {
    let result = await College.findByIdAndUpdate(collegeId, collegeData);
    res.json({
      success: true,
      message: "College updated by ID Successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteCollege = async (req, res, next) => {
  let collegeId = req.params.collegesId;

  try {
    let result = await College.findByIdAndDelete(collegeId);
    res.json({
      success: true,
      message: "College deleted successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
