import { serverLink } from "../constant.js"

export let handleFile = (req,res)=>{
    // console.log(req.files)
      let links = req.files.map((value,i)=>{
      return(`${serverLink}/${value.filename}`)
    })
  
    res.json({
      success:true,
      message:"File created successfully",
      result:links
    })
  }