import { Webuser } from "../schema/model.js"


export let createWebuser = async(req,res,next)=>{

    let WebuserData = req.body

    try {
        let result = await Webuser.create(WebuserData)
        res.json({
            success:true,
            message:"Webuser Created Successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
  
}

export let readWebuser = async(req,res,next)=>{
    try {
                // Find-->Sort-->Select-->Skip-->Limit (whatever be the order)

        // let result = await Webuser.find({})
        // Sorting(Unlike JS, mongoDB sorting works for number)
        // let result = await Webuser.find({}).sort("name") // ascending sort
        // let result = await Webuser.find({}).sort("-name") // descending sort
        // let result = await Webuser.find({}).sort("name age") // sort by name, then sort by age(ascending) that has same name
        // let result = await Webuser.find({}).sort("name -age") // sort by name, then sort by age(descending) that has same name

        // Select

        // Find has control over the object but Select has control over the object property

        let result = await Webuser.find({}).select("name age -_id")


        // Pagination
        // Skip

        // let result = await Webuser.find({}).skip("3")

        // Limit

        // let result = await Webuser.find({}).limit("5")

        // let result = await Webuser.find({}).limit("5").skip("2")

        res.json({
            success:true,
            message:"Webuser Read Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let readWebuserDetail = async(req,res,next)=>{
    let WebuserId = req.params.WebusersId

    try {
        let result = await Webuser.findById(WebuserId)
        res.json({
            success: true,
            message: "Webuser Read by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteWebuser = async(req,res,next)=>{
    let WebuserId = req.params.WebusersId

    try {
        let result = await Webuser.findByIdAndDelete(WebuserId)
        res.json({
            success:true,
            message:"Webuser Deleted Successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateWebuser = async(req,res,next)=>{
    let WebuserId = req.params.WebusersId
    let WebuserData = req.body
    try {
        let result = await Webuser.findByIdAndUpdate(WebuserId,WebuserData)
        res.json({
            success:true,
            message:"Webuser Updated by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}