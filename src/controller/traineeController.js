import { Trainee } from "../schema/model.js"


export let createTrainee = async(req,res,next)=>{

    let traineeData = req.body

    try {
        let result = await Trainee.create(traineeData)
        res.json({
            success:true,
            message:"Trainee Created Successfully"
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
  
}

export let readTrainee = async(req,res,next)=>{
    try {
        let result = await Trainee.find({})
        res.json({
            success:true,
            message:"Trainee Read Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let readTraineeDetail = async(req,res,next)=>{
    let traineeId = req.params.traineesId

    try {
        let result = await Trainee.findById(traineeId)
        res.json({
            success: true,
            message: "Trainee Read by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteTrainee = async(req,res,next)=>{
    let traineeId = req.params.traineesId

    try {
        let result = await Trainee.findByIdAndDelete(traineeId)
        res.json({
            success:true,
            message:"Trainee Deleted Successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateTrainee = async(req,res,next)=>{
    let traineeId = req.params.traineesId
    let traineeData = req.body
    try {
        let result = await Trainee.findByIdAndUpdate(traineeId,traineeData)
        res.json({
            success:true,
            message:"Trainee Updated by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}