import { User } from "../schema/model.js";
import bcrypt from "bcrypt";
import { sendEmail} from "../utils/sendEmail.js";
import { secretKey } from "../constant.js";
import jwt from "jsonwebtoken"
// async functions are promises

export let createUser = async (req, res, next) => {
  let userData = req.body;
  let password = userData.password;

  try {
    let hashPassword = await bcrypt.hash(password, 10);
    userData.password = hashPassword;
    let result = await User.create(userData);
    await sendEmail(
      {
        from:"AK <anujkoirala01@gmail.com>", // This part is displayed on the receiver head
        to:[req.body.email], // can send to multiple
        subject:"Email Verification",
        html:`<h1>You have been successfully registered.</h1>`
        // cc:[],
        // bcc:[],
        // attachments:{
        //   filename:'',
        //   path: ""
        // }

      }
    )
    res.status(201).json({
      success: true,
      message: "User created successfully",
      result: result,
    });
  } catch (error) {
    res.status(409).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUser = async (req, res, next) => {
  // let query = req.query;
  // let brake = query.brake;
  // let page = query.page;

  // let userData = req.body
  // let password = userData.password
  try {
    // let result = await User.find({});
    // let result = await User.find(query)
    let result = await User.find({});
    // .skip((page - 1) * brake)
    // .limit(brake);

    res.status(200).json({
      success: true,
      message: "User read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readUserDetail = async (req, res, next) => {
  let userId = req.params.usersId;

  try {
    let result = await User.findById(userId);
    res.status(200).json({
      success: true,
      message: "Student Read by ID Successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateUser = async (req, res, next) => {
  let userId = req.params.usersId;
  let userData = req.body;

  try {
    let result = await User.findByIdAndUpdate(userId, userData);
    res.status(201).json({
      success: true,
      message: "User updated by ID Successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUser = async (req, res, next) => {
  let userId = req.params.usersId;

  try {
    let result = await User.findByIdAndDelete(userId);
    res.status(200).json({
      success: true,
      message: "User deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUser = async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  try {
    let user = await User.findOne({ email: email });

    if (user === null) {
      res.json({
        success: false,
        message: "Email or Password does not match",
      });
    } else {
      let hashPassword = user.password;  //undefined
      let isValidPassword = await bcrypt.compare(password, hashPassword);
      if (isValidPassword) {

        let infoObj = {
          id:user._id
        }

        let expiryInfo={
          expiresIn:"365d"
        }

        let token = jwt.sign(infoObj,secretKey,expiryInfo)


        res.json({
          success: true,
          message: "User Logged in Successfully",
          result:token
        });

      } else {
        res.json({
          success: false,
          message: "Email or Password does not match",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};


export let myProfile = async(req,res)=>{
  let bearerToken = req.headers.authorization
  let token = bearerToken.split(" ")[1]

  try {
  let infoObj = jwt.verify(token,secretKey)
  let id = infoObj.id
  let result = await User.findById(id)
  res.json({
    success:true,
    message:"Profile read successfully",
    result:result
  })

} catch (error) {
  res.json({
    success:false,
    message:error.message
  })
}
  

}


export let updateProfile = async(req,res)=>{
  let userData = req.body;
  let bearerToken = req.headers.authorization
  let token = bearerToken.split(" ")[1]

  try {
  let infoObj = jwt.verify(token,secretKey)
  let id = infoObj.id
  
  let result = await User.findById(id)
  res.json({
    success:true,
    message:"Profile read successfully",
    result:result
  })



} catch (error) {
  res.json({
    success:false,
    message:error.message
  })
}
  

}
/* 
post
get password and email
  password = req.body.password
  email = req.body.email
check email if it matches
try-catch:
try:
  let user = await User.findByOne({email:email})
  let hashPassword = user?.password
  if(user === null)
  {
    res.json({
      success:false,
      message:"Email or Password does not match"
    })
  }
  else
  {
    let isValidPassword = await bcrypt.compare(password,hashPassword)
    if(isValidPassword)
    {
      res.json({
        success:true,
        message:"User logged in Successfully"
      })
    }
    else
    {
      res.json({
        success:false,
        message:"Email or Password does not match"
      })
    }
  }
catch:
res.json({
  success:false,
  message:error.message
})
    */