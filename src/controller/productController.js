import { Product } from "../schema/model.js"


export let createProduct = async(req,res,next)=>{

    let ProductData = req.body

    try {
        let result = await Product.create(ProductData)
        res.json({
            success:true,
            message:"Product Created Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
  
}

export let readProduct = async(req,res,next)=>{
    try {
        let result = await Product.find({})
        res.json({
            success:true,
            message:"Product Read Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let readProductDetail = async(req,res,next)=>{
    let ProductId = req.params.productsId

    try {
        let result = await Product.findById(ProductId)
        res.json({
            success: true,
            message: "Product Read by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteProduct = async(req,res,next)=>{
    let ProductId = req.params.productsId

    try {
        let result = await Product.findByIdAndDelete(ProductId)
        res.json({
            success:true,
            message:"Product Deleted Successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateProduct = async(req,res,next)=>{
    let ProductId = req.params.productsId
    let ProductData = req.body
    try {
        let result = await Product.findByIdAndUpdate(ProductId,ProductData)
        res.json({
            success:true,
            message:"Product Updated by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}