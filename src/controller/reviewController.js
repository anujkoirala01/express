import { Review } from "../schema/model.js"


export let createReview = async(req,res,next)=>{

    let ReviewData = req.body

    try {
        let result = await Review.create(ReviewData)
        res.json({
            success:true,
            message:"Review Created Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }
  
}

export let readReview = async(req,res,next)=>{
    try {
        // let result = await Review.find({}).populate("productId") show all property of product
        let result = await Review.find({})
        .populate("productId","name price -_id")
        .populate("userId", "name email -_id")
        res.json({
            success:true,
            message:"Review Read Successfully",
            result:result
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let readReviewDetail = async(req,res,next)=>{
    let ReviewId = req.params.reviewsId

    try {
        let result = await Review.findById(ReviewId)
        res.json({
            success: true,
            message: "Review Read by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteReview = async(req,res,next)=>{
    let ReviewId = req.params.reviewsId

    try {
        let result = await Review.findByIdAndDelete(ReviewId)
        res.json({
            success:true,
            message:"Review Deleted Successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateReview = async(req,res,next)=>{
    let ReviewId = req.params.reviewsId
    let ReviewData = req.body
    try {
        let result = await Review.findByIdAndUpdate(ReviewId,ReviewData)
        res.json({
            success:true,
            message:"Review Updated by ID successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}