// make an express application
// attach port to that application

// alt + shift + o --> organized imports

import express, { json } from "express";

import connectToMongoDB from "./src/connectToDb/connectToMongoDB.js";
import bikeRouter from "./src/router/bikeRouter.js";
import bookRouter from "./src/router/bookRouter.js";
import collegeRouter from "./src/router/collegeRouter.js";
import firstRouter from "./src/router/firstRouter.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import schoolsRouter from "./src/router/schoolsRouter.js";
import studentRouter from "./src/router/studentRouter.js";
import teacherRouter from "./src/router/teacherRouter.js";
import traineesRouter from "./src/router/traineesRouter.js";
import userRouter from "./src/router/userRouter.js";
import vehiclesRouter from "./src/router/vehiclesRouter.js";
import webuserRouter from "./src/router/webUserRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import imageRouter from "./src/router/imageRouter.js";
import { port } from "./src/constant.js";

let expressApp = express();
expressApp.use(json());
// Make static folder
expressApp.use(express.static("./public"))

// http://localhost:8000/image/g1.jpg

expressApp.listen(port, () => {
  console.log(`express application is listening at port ${port}`);
});

connectToMongoDB();

expressApp.use("/firsts", firstRouter);

expressApp.use("/bikes", bikeRouter);

expressApp.use("/trainees", traineesRouter);

expressApp.use("/schools", schoolsRouter);

expressApp.use("/vehicles", vehiclesRouter);

expressApp.use("/students", studentRouter)

expressApp.use("/teachers",teacherRouter)

expressApp.use("/books",bookRouter)

expressApp.use("/users", userRouter)

expressApp.use("/colleges", collegeRouter)

expressApp.use("/webusers", webuserRouter)

expressApp.use("/products", productRouter)

expressApp.use("/reviews", reviewRouter)

expressApp.use("/files",fileRouter)

expressApp.use("/images",imageRouter)

// Generate Hash Password

// let password = "abcde"
// let hashPassword = await bcrypt.hash(password,10)
// console.log(hashPassword)


// let hashPassword = "$2b$10$cqeITMXmR6stteL8Cw7RlOHSKDaLu8yOGc2lJ0oyMCTx1daWAquUa"

// let password = "abcde"

// let isValidPassword = await bcrypt.compare(password,hashPassword)
// console.log(isValidPassword)


//localhost:8000/users/login
// method: post
// data : {email:,password:}



// Token (jsonWebToken:jwt)

/*  let infoObj = {
    id :"23112" // generally add id only
   name:"Anuj",
   age:24
 }

 let secretKey = "dw10"

 let expiryInfo={
   expiresIn:"365d"
 }

 let token = jwt.sign(infoObj,secretKey,expiryInfo)
 console.log(token) */





// Token Verify

// To be verified
  // token must be made form the given secretKey
  // must not expire

  // if token is valid -->infoObj
  // if token is not valid --> error

// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQW51aiIsImFnZSI6MjQsImlhdCI6MTcwMjAyMTg0MywiZXhwIjoxNzMzNTU3ODQzfQ.ZIYpSCIaV5CI9DyJn-cFmgPDJKo7GOxJouTAd-g93vQ"

// try {
//   let infoObj = jwt.verify(token,"dw10")
//   console.log(infoObj)
  
// } catch (error) {
//   console.log(error.message)
// }


// let files = [
//     {
//       destination: "./public",
//       filename: "abc.jpg",
//     },
//     {
//       destination: "./public",
//       filename: "nitan.jpg",
//     },
//     {
//       destination: "./public",
//       filename: "ram.jpg",
//     },
//   ];
  

//   let link = files.map((value,i)=>{
//     return(`http://localhost:8000/${value.filename}`)
//   })

//   console.log(link)
  
  // [
  //   "http://localhost:8000/abc.jpg",
  //   "http://localhost:8000/nitan.jpg",
  //   "http://localhost:8000/ram.jpg",
  // ]


// url = localhost:8000/product?address=USA
// url = route?query
// route = localhost:8000/product/a/b
// route = baserUrl/routeParameter1/routeParameter2
