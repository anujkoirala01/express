/* alt + shift + a --> comment box
alt + shift + o --> organized imports

Middleware are the function which has req, res and next.

To trigger another middleware, we have to call next().


Middleware is divided into two parts:
1. Normal Middleware : 
    (req,res,next)=>{}
    To trigger next normal middleware, we have to call next()

2. Error Middleware :
    (err,req,res,next)=>{}
    To trigger next error middleware, we have to call next("/string or anything, generally we pass error/") 


Mongoose is used to connect DB and backend.
    npm i mongoose


Connect our application to mongoDB using



Asynchronous code --> execute at last 
    To execute it at first add 'await' in front of the code and put 'async' in function
    let connectToMongoDB = async () => {

    await mongoose.connect("mongodb://0.0.0.0:27017");
    


Before adding data to database --> First we have to define structure of data
        define object --> schema
        define array: name and object --> model
    }



dw-09:

CRUD : Create, Read, Update, Delete --> Post, Get, Patch, Delete (Method)

Backend : Make API --> Defining task for each request is called making API  

Postman : Hit API and Test API

If you send data from body(Postman), always use post or patch.

Data from postman can be sent in 3 different ways:
    body
    query
    params

Whatever we pass in query, it is in string datatype

Route parameter is divided into 2 parts:
 static parameter : we have to use same name as we mention 
 dynamic parameter : we can use any name

Always place API, which has dynamic route parameter, at last



Schema Design:
    Manipulation
    Validation


Searching(Query/Search):
    Type does not matter in searching, but value does

    Regex Searching(Not Exact Searching)



Find has control over the object but Select has control over the object property



1+1=2
"1"+"1"="11"
"1"+1="11"

1-1=0
"1"-1=> 1-1 = 0


Dynamic Route at last




BackEnd:
    api
    crud
    hashing
    jwt
    convert file to link
    save file to server
    send email



Login User:
    post email, password  =
    backend get email, password=
    find the user whose email is (postman email)=
    if not   => success:false, message: "Email or Password does not match"
    if user exist
      check password match (using bcrypt)
      if password does not math   => success:false, message: "Email or Password does not match"
      if password match => success:true, message:User logged in successfully.

Token (jsonWebToken:jwt)
    Generate
        detail
        logo
        exp date(info)
    Validate


Handle File
    static folder : file can be accessed using link


Error : Cannot set headers after they are sent to the client
    i.e there are 2 response
    But a req must have a single res


Multer is package which takes images from frontend and add images to the public folder


In form-data, we can get file information through req.files whereas we can get other information
via req.body



Email:
    login
        email, password
    to => multiple
    subject
    body(html)



Error: error has occurred transporter.sendEmail is not a function (on Console)
    It is 'sendMail' not 'sendEmail'
    

backend
   .env
    status code



Status Code:
    Success: 2XX (Default: 200)
        Get, Delete: 200
        Post, Patch: 201
    Failure: 4XX (Default: 400)
        404 -> Not Found
        409 -> Conflict
    Internal Server Error: 500 (Error in backend code)


.env
    Always make .env file in root folder
    In .env we define  special variable (credential(email, password, secretKey) , port , links (database, http://localhost:8000))
    Variable name must be in upper case
    Separate variable name using underscore
    All variable value in .env is string


register
    data 
    hash password
    save
    send email



login
    email, password
    if user with that email exist
    if user exist check password match or not
    if match generate token 
    send token to front end(postman)



my profile
    pass token from postman [Authorization --> Bearer Token(type)-->send]
    get token in backend
    validate token
    if not validated, throw error
    else, show my-profile


update profile
update password
*/
